import asyncio

import websockets
connected = []
message = ""
async def handler(websocket):
    global connected, message
    connected.append(websocket)
    await websocket.send(message)
    while True:
        message = await websocket.recv()
        faild = []
        for v,i in enumerate(connected):
            try:
                await i.send(message)
            except:
                faild.append(v)
        i = 0 
        for z in faild:
            try:
                connected.pop(z+i)
                i += 1
            except:
                pass
        print(message)



async def main():
    async with websockets.serve(handler, "", 8000):
        await asyncio.Future()  # run forever

if __name__ == "__main__":
    asyncio.run(main())
